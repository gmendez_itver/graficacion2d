
import java.awt.Graphics2D;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Laboratorio
 */
public class Triangulo extends Figura {
    Linea lado1;
    Linea lado2;
    Linea lado3;
    
    public Triangulo(Linea l1, Linea l2, Linea l3){
        this.lado1 = l1;
        this.lado2 = l2;
        this.lado3 = l3;
    }
    
    public void dibujar(Graphics2D g2){
        lado1.dibujar(g2);
        lado2.dibujar(g2);
        lado3.dibujar(g2);
    }
}
