
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Laboratorio
 */
public class Linea extends Figura {
    Point pA = null;
    Point pB = null;
 
    public Linea(Point pA, Point pB){
        this.pA = pA;
        this.pB = pB;
    }
    
    public Linea(Point pA, Point pB, Color c){
        this.pA = pA;
        this.pB = pB;
        this.c = c;
    }
    
    public Linea(int x1, int y1, int x2, int y2){
        this.pA = new Point(x1,y1);
        this.pB = new Point(x2,y2);
    }    
    
    public Linea(int x1, int y1, int x2, int y2, Color c){
        this.pA = new Point(x1,y1);
        this.pB = new Point(x2,y2);
        this.c = c;
    }    
    
    Point getPointA(){
        return this.pA;
    }

    Point getPointB(){
        return this.pB;
    }    
        
    public void dibujar(Graphics2D g2){
        Color tmp = g2.getColor();
        
        if (this.c!=null){
            g2.setColor(c);
        }
        
        g2.drawLine((int)pA.getX(), (int)pA.getY(), 
                    (int)pB.getX(), (int)pB.getY());
        
        g2.setColor(tmp);
    }
}
